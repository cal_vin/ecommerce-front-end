import axios from 'axios';
import React from 'react';
import config from '../config/config';
import { UserInfo } from '../domain/backendAuthDo';
import { UserInfoResponseDto } from '../domain/dto/backendAuthDtos';

export default class BackendExtService {

    // need to send User idToken in each Request Header
    static getUserInfo(idToken: string, callback: (data: UserInfo) => void) {
        axios.get<UserInfoResponseDto>(config().backend.baseUrl + "/user/info/me", {
            headers: {
                Authorization: "Bearer " + idToken
            }
        })
            .then( (response) => {
                callback(response.data as UserInfo);
            });
    }
}
