import React from 'react';
import { PaymentInformation, ProductItem, ProductList, Transaction, TransactionItem, TransactionItemMap } from '../domain/backendDos';
import { CheckoutResponseDto, ProductDetailResponseDto, ProductListResponseDto, ShoppingCartItemsResponseDto, TransactionResponseDto } from '../domain/dto/backendDtos';
import { ShoppingCartItem } from '../domain/shoppingCartDos';

import mockProductList from './products.json';
import mockProductDetail from './productDetail.json';
import mockShoppingCartItems from './shoppingCartItems.json';
import mockCheckout from './checkout.json';
import axios, { AxiosResponse } from 'axios';
import config from '../config/config';
import { CommentResponseDto, CommentResponseDtoList } from '../domain/dto/backendCommentDto';
import { CommentDo, CommentDos, NewCommentDo } from '../domain/commentDos';


export default class BackendExtService {

    // fetchAllProduct
    static getProductList(callback: (data: ProductList) => void) : void {
        const promise: Promise<AxiosResponse<ProductListResponseDto>> = 
            axios.get<ProductListResponseDto>(config().backend.baseUrl + "/public/product/all");

        promise.then(
            (response: AxiosResponse<ProductListResponseDto>) => {
                if (response.status === 200) {
                    callback(response.data as ProductList);
                }
            }
        );
    }


    // Mock up calling API
    // static getProductList(callback: (data: ProductList) => void) {
        
    //     // JS built-in: Promise, setTimeout(); no need to import
    //     new Promise((resolve, reject) => {
    //         setTimeout(() => {
    //             resolve(mockProductList as ProductListResponseDto)
    //         }, 1000);
    //     }).then(data => {
    //         callback(data as ProductList);
    //     });
    // }




    // fetchProductDetails
    static getProductDetail(productId: string, callback: (data: ProductItem) => void ) {
        axios.get<ProductDetailResponseDto>(
            config().backend.baseUrl + "/public/product/?productId=" + productId
        ).then(
            (response: AxiosResponse<ProductDetailResponseDto>) => {
                if (response.status === 200) {
                    callback(response.data as ProductItem);
                }
            }
        );
    }

    // createComment
    static createComment(idToken: string, newComment: NewCommentDo, callback: (data: CommentDo) => void) {
        axios.post<CommentResponseDto>(config().backend.baseUrl + "/comment/create", newComment, {
            headers: {
                Authorization: "Bearer " + idToken
            }
        }).then(
            (response: AxiosResponse<CommentResponseDto>) => {
                if (response.status === 200) {
                    callback(response.data as CommentDo);
                }
            }
        );
    }

    // fetchProductComment
    static getProductComments(productId: string, callback: (data: CommentDos) => void) {
        axios.get<CommentResponseDtoList>(
            config().backend.baseUrl + "/public/comment/all?productId=" + productId
        ).then(
            (response: AxiosResponse<CommentResponseDtoList>) => {
                if (response.status === 200) {
                    callback(response.data as CommentDos);
                }
            }
        );
    }

    // fetchProductsByIds
    static getShoppingCartItems(cart: {[key: string] : ShoppingCartItem}, callback: (data: TransactionItemMap) => void) {

        const productIdsInCart = Object.keys(cart);

        axios.post<ShoppingCartItemsResponseDto>(config().backend.baseUrl + "/public/product", productIdsInCart)
            .then( (response: AxiosResponse<ShoppingCartItemsResponseDto>) => {

                if (response.status === 200) {

                    const shoppingCartItemsResponseDto = response.data as ShoppingCartItemsResponseDto;

                    let transactionItemnMap: {
                        [key: string] : TransactionItem
                    } = {};

                    for (let DtoKey of Object.keys(shoppingCartItemsResponseDto) ) {
                        for (let cartKey of productIdsInCart ) {
                            if (DtoKey === cartKey) {

                                const price = shoppingCartItemsResponseDto[DtoKey].price;
                                const quantity = cart[DtoKey].quantity;

                                transactionItemnMap[cartKey] = {
                                    details: shoppingCartItemsResponseDto[DtoKey] as ProductItem,
                                    quantity: quantity,
                                    subtotal: quantity * price
                                }
                            }
                        }
                    }

                    callback(transactionItemnMap as TransactionItemMap);
                }
            })
    }
    

    // createTransaction
    static checkout(idToken: string, items: ShoppingCartItem[], callback: (data: Transaction) => void ) {
        axios.post<CheckoutResponseDto>(config().backend.baseUrl + "/transaction/create", items, {
            headers: {
                Authorization: "Bearer " + idToken
            }
        })
            .then( (response: AxiosResponse<CheckoutResponseDto>) => {
                if (response.status === 200) {
                    callback(response.data as Transaction);
                }
            })
    }

    // getTransactionById
    static getTransaction(idToken: string, transactionId: number, callback: (data: Transaction) => void) {
        axios.get<TransactionResponseDto>(config().backend.baseUrl + "/transaction/get/" + transactionId, {
            headers: {
                Authorization: "Bearer " + idToken
            }
        })
            .then((response: AxiosResponse<TransactionResponseDto>) => {
                if (response.status === 200) {
                    callback(response.data as Transaction);
                }
            })   
    }

    // completeTransaction
    static completeTransaction(idToken: string, paymentInformation: PaymentInformation, callback: (data: Transaction) => void) {
        axios.patch<TransactionResponseDto>(config().backend.baseUrl + "/transaction/complete", paymentInformation, {
            headers: {
                Authorization: "Bearer " + idToken
            }
        })
            .then( (response: AxiosResponse<TransactionResponseDto>) => {
                if (response.status === 200) {
                    callback(response.data as Transaction);
                }
            })
    }

}