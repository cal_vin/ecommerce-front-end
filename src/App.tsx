import React from 'react';
import './App.css';
import { HashRouter, Redirect, Route, Switch } from 'react-router-dom';
import ProductListingPage from './ui/page/ProductListingPage';
import ProductDetailPage from './ui/page/ProductDetailPage';
import { Nav, Navbar, Spinner } from 'react-bootstrap';
import ShoppingCartService from './service/ShoppingCartService';
import ShoppingCartPage from './ui/page/ShoppingCartPage';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart, faSignOutAlt, faUser } from '@fortawesome/free-solid-svg-icons';
import CheckoutPage from './ui/page/CheckoutPage';
import AuthService from './service/AuthService';
import LoginPage from './ui/page/LoginPage';
import TransactionCompletedPage from './ui/page/TransactionCompletedPage';
import LandingPage from './ui/page/LandingPage';
import RegistryPage from './ui/page/RegistryPage';

type Props = {}; // save things from superclass
type State = {
  isLoading: boolean;
  isOnLandingPage: boolean;
}; // save things from its own Ui

// change App() function to React.Component/Class for including shoppingCart Instance
export default class App extends React.Component<Props, State> {
  state = {
    isLoading: false,
    isOnLandingPage: false
  } as State;


  componentDidCatch(e:Error) {
    alert(e.message);
    console.log('error: ', e);
  }

  shoppingCartService: ShoppingCartService;
  authService: AuthService;

  constructor(props: Props) {
    super(props);

    this.shoppingCartService = new ShoppingCartService();

    // bind the onAuthStateChange before give others
    this.onAuthStateChange = this.onAuthStateChange.bind(this);
    this.onLandingPage = this.onLandingPage.bind(this);
    // Initial oAuth App
    this.authService = new AuthService(this.onAuthStateChange);
    this.onClickSignOut = this.onClickSignOut.bind(this);

    this.authService.init();
  }

  componentDidMount() {}

  onAuthStateChange(isLoading: boolean) {
    this.setState({
      isLoading: isLoading
    });
  }

  onLandingPage(isOnLandingPage: boolean) {
    this.setState({
      isOnLandingPage: isOnLandingPage
    });
  }

  onClickSignOut(event: React.MouseEvent<any>) {
    event.preventDefault();
    this.authService.signOut(() => {
      window.location.href = '#/login';
    });
  }

  render() {
    let loadingOverlayClassName = "loadingOverlay";
    if (this.state.isLoading) {
      loadingOverlayClassName += " active";
    }
    

    return (
      <div className="App">
          <Navbar bg="light" expand="lg" sticky="top" id = "navbar">
            <Navbar.Brand href="#home" id = "head">Cal_tail</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            {/* make it on right side by collapse */}
            <Navbar.Collapse className="justify-content-end">

              <Nav.Link href="#/cart">
                <h6 className= "icon-label">ShoppingCart</h6>
                <FontAwesomeIcon icon={faShoppingCart} />
              </Nav.Link>

              {
                (AuthService.isLogin()) ? (
                  <Nav.Link onClick={this.onClickSignOut}>
                    <h6 className= "icon-label">Logout</h6>
                    <FontAwesomeIcon icon={faSignOutAlt} />
                  </Nav.Link>
                ) : (
                  <Nav.Link href="#/login">
                    <h6 className= "icon-label">Login</h6>
                    <FontAwesomeIcon icon={faUser} />
                  </Nav.Link>
                )
              }


            </Navbar.Collapse>

          </Navbar>

        <HashRouter>
          {/* Bootstrap Switch, not react Switch */}
          <Switch>
            {/* 
              add "#" before path: http://localhost:3000/#/ 
              "/" is root page,
              the previous page will unamount once render to new page by new path
            */}
            <Route exact path="/">
              <Redirect to="home"></Redirect>
            </Route>

            <Route exact path="/home">
              <LandingPage
                onLandingPag={this.onLandingPage}
              />
            </Route>

            <Route exact path="/registry">
              <RegistryPage
                authService={this.authService}
              />
            </Route>
            
            <Route exact path="/login">
              <LoginPage
                authService={this.authService}
              />
            </Route>

            <Route exact path="/list">
              <ProductListingPage />
            </Route>

            {/* react-route provide pathvairable by ":" , 
            any Input after : here will become productId automatically*/}
            <Route exact path="/detail/:productId">
              {/* Once the exact path match the pattern, DetailPage will render */}
              <ProductDetailPage
                // pass the Instance to DetailPage Instance as props
                shoppingCartService={this.shoppingCartService}
              // new ProductDetailPage({shoppingCartService: this.shoppingCartService})
              />
            </Route>

            <Route exact path="/cart">
              <ShoppingCartPage
                shoppingCartService={this.shoppingCartService}
              />
            </Route>

            <Route exact path="/checkout/:transactionId">
              <CheckoutPage />
            </Route>

            <Route exact path="/complete">
              <TransactionCompletedPage />
            </Route>

          </Switch>
        </HashRouter>

        { (this.state.isOnLandingPage) ? null : 
          
            <div id = "webpage-footer">
                <h6 id = "footer-text">@2021 Cal_tail</h6>
            </div>
        }

        <div className={loadingOverlayClassName}>

          <span className="loadingSpinner">
            <Spinner animation="border" role="status">
              <span className="sr-only">Loading...</span>
            </Spinner>
          </span>

        </div>

      </div>
    );
  }
}
