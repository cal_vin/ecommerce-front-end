import devConfig from "./devConfig";
import prodConfig from "./prodConfig";

export default function config() {
    // process.env.NODE_ENV remember wheter production of development
    if (process.env.NODE_ENV === "production") {
        // npm run build
        return prodConfig;
    } else {
        // npm start
        return devConfig;
    }
}