import React from 'react';

export default class LocalStorageUtil {
    static setValue(key: string, value: any) {
        // Built-in obejct in browser: localStorage
        // JSON.stringify(): Converts a JavaScript value to a JavaScript Object Notation (JSON) string.
        localStorage.setItem(key, JSON.stringify(value));
    }

    static getValue(key: string) {
        const value = localStorage.getItem(key); //  return string or "undefined" if empty

        if (value && value !== 'undefined') {
            return JSON.parse(value); // Converts a JavaScript Object Notation (JSON) string into an object
        }

        return undefined;
    }
}