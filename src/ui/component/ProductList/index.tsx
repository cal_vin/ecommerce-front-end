import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Fragment } from 'react';
import { Button, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { TransactionItemMap } from '../../../domain/backendDos';
import './style.css';

// props is read-only, only can change the props in child class by callback functino in props
// trigger new props and child class re-render
type Props = {
     // ? because the parmas given by others is optional
    displayItems?: TransactionItemMap,
    shouldShowRemoveButton: boolean,
    onClickDeleteItemButton?: (productId: string) => void
    shouldShowQuantityControls: boolean
    addQuantity?: (key:string) => void;
    minQuantity?: (key:string) => void;
};

type State = {};

export default class ProductList extends React.Component<Props, State> {
    
    renderListItems() {
        const cartItems: JSX.Element[] = [];

        if (!this.props.displayItems) {
            return null;
        }

        for (let productId of Object.keys(this.props.displayItems)) {
            const item = this.props.displayItems[productId];
            
            // for each {} in JSX, only return one element
            cartItems.push(

                
                <tr>
                    
                    {
                        (this.props.shouldShowQuantityControls) ? (
                            <Fragment>
                                <td className="Image-background"> 
                                {/* inlince CSS in JSX */}
                                    <Link to = {"/detail/" + item.details.id}>
                                        <img 
                                            className = "cartImg" 
                                            style={{"backgroundImage" : "url(" + item.details.imageUrl + ")"}}
                                        >
                                        </img>
                                    </Link>
                                </td>
                                <td>{item.details.name}</td>
                                <td>${item.details.price}</td>
                                <td>
                                    <button className="modifier left " onClick={() => this.props.minQuantity!(productId)}>
                                        &mdash;
                                    </button>

                                    <h3 className="screen" >
                                        {item.quantity} 
                                    </h3>

                                    <button className="modifier right " onClick={() => this.props.addQuantity!(productId)}>
                                        &#xff0b;
                                    </button>  
                                </td>
                            </Fragment>
                        ) : (
                            <>
                                <td className="Image-background">
                                    <img className = "cartImg" style={{"backgroundImage" : "url(" + item.details.imageUrl + ")"}}></img>
                                </td>
                                <td>{item.details.name}</td>
                                <td>${item.details.price}</td>
                                <td>{item.quantity}</td>

                            </>
                            )
                    }

                    <td>${item.subtotal}</td>
                    
                        {
                            (this.props.shouldShowRemoveButton) ? (
                               <td> 
                                    <Button 
                                        variant="secondary"
                                        // on Click receive function
                                        onClick = {(/* params given by the event */) => this.props.onClickDeleteItemButton!(productId)} 
                                        // when user click, call onClick()
                                    >
                                        <FontAwesomeIcon icon = {faTrashAlt}/>
                                    </Button>     
                                </td>
                            ) : null
                        } 
                </tr>
            );
            // this.state.totalPrice += item.price;
        }

        return cartItems;
    }
    
    
    
    render() {
        return (
            <Table striped bordered hover responsive="xl" className="">

                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Subtotal</th>

                        {
                            (this.props.shouldShowRemoveButton) ? (
                                <th>Action</th>
                            ) : null
                        }
                        
                    </tr>
                </thead>

                <tbody>

                    {this.renderListItems()}

                </tbody>

            </Table>
        )
    }
}