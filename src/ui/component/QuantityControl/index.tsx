import React from 'react';
import "./style.css";

type Props = {};
type State = {    
    quantity: number
};

export default class QuantityControl extends React.Component <Props, State>{
    state = {
        quantity: 1
    } as State

    constructor (props:Props){
        super(props);
        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
        this.onHandleQuantityChange = this.onHandleQuantityChange.bind(this);
    }

    increment() {
        this.setState(
            (prevState) => (
                {quantity: prevState.quantity < 99? +prevState.quantity +1 : 99}
            )
        );
        
    }
      
    decrement() {
        this.setState(
            (prevState) => (
                {quantity: prevState.quantity > 1? +prevState.quantity -1 : 1}
            )
        );
    }

    onHandleQuantityChange(event: React.ChangeEvent<HTMLInputElement>) {
        event.preventDefault()
        const target = event.target; // Input Text box, element
        const value = target.value; // user latest text box
        const name = target.name; // name of element
        
        const pattern = /^([1-9]|[1-9][0-9])$/;
        
        // invalid character, prevent input
        if (pattern.test(value)) {

            // @ts-ignore
            this.setState(() => (
                {
                    [name]: +value
                }
            
            ));
        }
  
    }

    getQuantity() {
        return this.state.quantity;
    }



    render(){
        
        return(
            <div id="quantityControl">

                <button className="modifier --left " onClick={this.decrement}>
                    &mdash;
                </button>

                <input 
                    className="screen" 
                    type = "text" 
                    name = "quantity"
                    value = {this.state.quantity} 
                    onChange={this.onHandleQuantityChange} 
                />
                
                <button className="modifier right " onClick={this.increment}>
                    &#xff0b;
                </button>  

            </div>
        );
    }
}