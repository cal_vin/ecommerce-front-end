import React from 'react';
import './style.css';
import { Button, Carousel, Container } from 'react-bootstrap';

type Props = {
    onLandingPag: (isOnLandingPag:boolean) => void;
};
type State = {};

export default class LandingPage extends React.Component<Props, State> {
    state = {} as State;
    constructor(props: Props) {
        super(props);
        this.props.onLandingPag(true);
    }

    componentWillUnmount() {
        this.props.onLandingPag(false);
    }

    onClickReturnButton() {
        window.location.href = "#/list";
    }

    render() {
        return (
            
            <div id="landingPageContainer">  

                <Container id = "landingPage">
                    <h1>Welcome!</h1>
                    <Carousel className="carousels">
                        <Carousel.Item>
                            <img
                                className="d-block w-100"
                                src="https://i.pinimg.com/564x/5b/13/3c/5b133c316e956ace32d77425552ac7f0.jpg"
                                alt="First slide"
                            />
                            <Carousel.Caption>
                                <h5>First slide label</h5>
                            </Carousel.Caption>
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                                className="d-block w-100"
                                src="https://i.pinimg.com/564x/98/4d/86/984d86af17f0d4a02381122540a8c3c2.jpg"
                                alt="Second slide"
                            />

                            <Carousel.Caption>
                                <h5>Second slide label</h5>
                            </Carousel.Caption>
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                                className="d-block w-100"
                                src="https://i.pinimg.com/564x/f5/bd/c0/f5bdc08db59c827b8e6a6063262d4d22.jpg"
                                alt="Third slide"
                            />

                            <Carousel.Caption>
                                <h5>Third slide label</h5>
                            </Carousel.Caption>
                        </Carousel.Item>
                    </Carousel>


                    <Button 
                        className = "button"
                        variant="dark"
                        size="lg"
                        onClick = {() => this.onClickReturnButton()}
                        block
                    >
                        <b>Go Shopping Now !</b>
                    </Button>  

                </Container>
            </div>
        );
    }
}