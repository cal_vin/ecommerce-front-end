import React from 'react';
import { Button, Container } from 'react-bootstrap';
import './style.css';

type Props = {};
type State = {};

export default class TransactionCompletedPage extends React.Component<Props, State> {
   
    onClickReturnButton() {
        window.location.href = "#/";
    }
    
   
    render() {
        return (
            <div id="completePageBackgroundImage">
                <Container id="completePage">

                    <div className="content">
                        <h3>
                            Thank you for your Payment !
                        </h3>
                        <div className = "button">
                            <Button 
                                    variant="dark"
                                    size="lg"
                                    onClick = {() => this.onClickReturnButton()}
                                    block
                                >
                                    <b>Return</b>
                            </Button> 
                        </div> 
                    </div>

                </Container>
            </div>
        );
    }
}