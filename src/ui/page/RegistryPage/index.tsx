import React from 'react';
import './style.css';
import { Button, Container, Form } from 'react-bootstrap';

import AuthService from '../../../service/AuthService';

type Props = {
    authService: AuthService
};
type State = {
    email: string,
    password: string,
    confirmPassword: string
};

// React Terminology: Fully controlled component
export default class RegistryPage extends React.Component<Props, State> {
    // Initial state
    state = {
        email: "",
        password: "",
        confirmPassword: ""
    } as State;

    constructor(props: Props) {
        super(props);
        if (AuthService.isLogin()) {
            window.location.href = "#/home";
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.onRegistry = this.onRegistry.bind(this);
    }

    handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
        const target = event.target; // Input Text box, element
        const value = target.type === 'checkbox' ? target.checked : target.value; // user latest check box
        const name = target.name; // name of element
        // @ts-ignore
        this.setState({
          [name]: value
        });
    
    }

    onRegistry(event: React.FormEvent<HTMLFormElement>): void {
        event.preventDefault();

        if (this.state.confirmPassword === this.state.password) {
            this.props.authService.registryWithEmailPassword(this.state.email, this.state.password, this.onRegistrySuccess, this.onRegistryError);
        } else {
            alert("Password should be the same, please confirm your password !");
        }
    }

    onRegistrySuccess() {
        window.location.href = "#/";
    }

    onRegistryError(code: string, message: string): void {
        alert("Error:  "+ code + "/ " + message + ",  please try again !");
    }

    onClickReturnButton() {
        alert("Notice: You have to Login before CheckOut and making comments !");
        window.location.href = "#/";
    }
    

    render() {
        return (
            <div className="registryPage-background">

                <Container id = "registryPage">

                    <div className="loginPanel">
                        <h1>Registry</h1>

                        <Form onSubmit={this.onRegistry}>

                            <Form.Group>
                                <Form.Label>Email address</Form.Label>
                                <Form.Control 
                                    type="email" 
                                    placeholder="Enter Your email"
                                    name = "email"
                                    onChange = {this.handleInputChange}
                                    value = {this.state.email}
                                />
                                <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Enter Your Password"
                                    name = "password"
                                    onChange = {this.handleInputChange}
                                    value = {this.state.password}
                                />
                                <Form.Text className="text-muted">
                                    * Password Should Be At Least 6 Characters
                                </Form.Text>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Confirm password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Enter Your Password Again"
                                    name = "confirmPassword"
                                    onChange = {this.handleInputChange}
                                    value = {this.state.confirmPassword}
                                />
                                <Form.Text className="text-muted">
                                    * Password Should The Same
                                </Form.Text>
                            </Form.Group>

                            <Button 
                                className="submitButton"
                                variant="primary" 
                                // trigger the submit event in <Form/>
                                type="submit"
                                size="lg" block
                            >
                                Submit
                            </Button>
                        </Form>

                        <Button 
                            className="submitButton"
                            onClick={() => this.onClickReturnButton()}
                            variant="outline-secondary" 
                            // trigger the submit event in <Form/>
                            type="submit"
                            size="lg" block
                        >
                            Registry Later
                        </Button>

                    </div>

                </Container>
            
            </div>
        );
    }
}