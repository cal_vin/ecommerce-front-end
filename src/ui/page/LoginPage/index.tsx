import React from 'react';
import './style.css';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import GoogleButton from 'react-google-button';
import { FacebookLoginButton } from 'react-social-login-buttons';
import AuthService from '../../../service/AuthService';
import { Link } from 'react-router-dom';

type Props = {
    authService: AuthService
};
type State = {
    email: string,
    password: string
};

// React Terminology: Fully controlled component
export default class LoginPage extends React.Component<Props, State> {
    // Initial state
    state = {
        email: "",
        password: ""
    } as State;

    constructor(props: Props) {
        super(props);
        if (AuthService.isLogin()) {
            window.location.href = "#/home";
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.onLogin = this.onLogin.bind(this);
        this.onSignInWithGoogle = this.onSignInWithGoogle.bind(this);
        this.onSignInWithFacebook = this.onSignInWithFacebook.bind(this);
    }

    handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
        const target = event.target; // Input Text box, element
        const value = target.type === 'checkbox' ? target.checked : target.value; // user latest check box
        const name = target.name; // name of element
        // @ts-ignore
        this.setState({
          [name]: value
        });
    
    }

    onLogin(event: React.FormEvent<HTMLFormElement>): void {
        event.preventDefault();

        this.props.authService.signInWithEmailPassword(this.state.email, this.state.password, this.onSignInSuccess, this.onSignInError);
    }

    onSignInWithGoogle(event: React.MouseEvent<HTMLDivElement>): void {
        event.preventDefault();

        this.props.authService.signInWithGoogle(this.onSignInSuccess, this.onSignInError);
    }


    onSignInWithFacebook() {
        alert("NOTICE: According to facebook security policy, facebook login option is diable temporarily, please use other option, thank you ~")
        // event.preventDefault();
        // this.props.authService.signInWithFacebook(this.onSignInSuccess, this.onSignInError);
    }

    onSignInSuccess() {
        window.location.href = "#/";
    }

    onSignInError(code: string, message: string): void {
        alert("Error:  "+ code + "/ " + message);
    }

    onClickReturnButton() {
        alert("Notice: You have to Login before CheckOut and making comments !");
        window.location.href = "#/";
    }
    

    render() {
        return (
            <div className="loginPage-background">

                <Container id = "loginPage">

                    <div className="loginPanel">
                        <h1>Login</h1>

                        <Form onSubmit={this.onLogin}>

                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control 
                                    type="email" 
                                    placeholder="Enter Your email"
                                    name = "email"
                                    onChange = {this.handleInputChange}
                                    value = {this.state.email}
                                />
                                <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text>
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Enter Your Password"
                                    name = "password"
                                    onChange = {this.handleInputChange}
                                    value = {this.state.password}
                                />
                            </Form.Group>

                            <Button 
                                className="submitButton"
                                variant="primary" 
                                // trigger the submit event in <Form/>
                                type="submit"
                                size="lg" block
                            >
                                Submit
                            </Button>
                        </Form>

                        <h5>OR</h5>

                        <Row>
                            <Col md = {6}  >
                                <GoogleButton
                                    className="googleButton"
                                    onClick = {this.onSignInWithGoogle}>
                                    <b>Sign in With Google</b>
                                </GoogleButton>
                            </Col>
                            <Col md = {6} >
                                <FacebookLoginButton
                                    align="center"
                                    className="facebookButton"
                                    onClick={this.onSignInWithFacebook}>
                                    <span style={{fontSize:"16px", textAlign: "center"}}>Facebook Sign In</span>
                                </FacebookLoginButton>
                            </Col>
                        </Row>

                        <br/>
                        <h5>Didn't have account?</h5> 
                        <Link to = {"/registry"}>                 
                            <h5>Registry Here</h5>
                        </Link>

                        <Button 
                            className="submitButton"
                            onClick={() => this.onClickReturnButton()}
                            variant="outline-secondary" 
                            // trigger the submit event in <Form/>
                            type="submit"
                            size="lg" block
                        >
                            Login Later
                        </Button>

                    </div>

                </Container>
            
            </div>
        );
    }
}