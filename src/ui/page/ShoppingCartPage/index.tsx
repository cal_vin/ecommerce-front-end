import { faGripHorizontal, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { Breadcrumb, Button, Container, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { TransactionItemMap, Transaction } from '../../../domain/backendDos';
import { ShoppingCartItem } from '../../../domain/shoppingCartDos';
import BackendExtService from '../../../extService/BackendExtService';
import AuthService from '../../../service/AuthService';
import ShoppingCartService from '../../../service/ShoppingCartService';
import ProductList from '../../component/ProductList';
import './style.css';

type Props = {
    shoppingCartService: ShoppingCartService
};
type State = {
    shoppingCartItems?: TransactionItemMap
};

export default class ShoppingCartPage extends React.Component<Props,State> {
    state = {} as State; // Initial State
    
    // default constructor if not specify:  constructor(props: Props) { super(props) };
    constructor(props: Props) {
        super(props);

        this.onLoadedShoppingCartItems = this.onLoadedShoppingCartItems.bind(this);
        this.onClickDeleteItemButton = this.onClickDeleteItemButton.bind(this);
        // Arrow function no need to bind
        // this.onClickCheckOutButton = this.onClickCheckOutButton.bind(this);
        this.addQuantity = this.addQuantity.bind(this);
        this.minQuantity = this.minQuantity.bind(this);
        this.onCreatedTransaction = this.onCreatedTransaction.bind(this);
    }
    
    componentDidMount() {
        const cart = this.props.shoppingCartService.getCart();
        // retrive all keys from object cart, return String Array
        BackendExtService.getShoppingCartItems(cart, this.onLoadedShoppingCartItems)    
    }

    onLoadedShoppingCartItems(data: TransactionItemMap) {

        this.setState({ // might check memory address if changed
            shoppingCartItems: data
        });
    }

    onClickDeleteItemButton(productId: string) {

        // remove the item from localStorage
        this.props.shoppingCartService.removeFromCart(productId);
        
        // remove from State
        // v1
        // Copy everything inside this.props.displayItems out 
        // to a new object, change memory address

        // const shoppingCartItems = {
        //     ...this.state.shoppingCartItems
        // };
        
        // delete shoppingCartItems[productId];
        // this.setState({
        //     shoppingCartItems: shoppingCartItems
        // });


        // v2

        delete this.state.shoppingCartItems![productId]; // like delete a item in a copy version

        // update the state by only using setState
        this.setState({
            shoppingCartItems: this.state.shoppingCartItems
        });

    }

    addQuantity(productId: string) {
        this.props.shoppingCartService.incrementOnCart(productId);

        this.state.shoppingCartItems![productId].quantity++;
        this.state.shoppingCartItems![productId].subtotal += this.state.shoppingCartItems![productId].details.price; 

        this.setState({
            shoppingCartItems: this.state.shoppingCartItems
        });

    }

    minQuantity(productId: string) {
        this.props.shoppingCartService.decrementOnCart(productId);

        if (this.state.shoppingCartItems![productId].quantity > 1) {
            this.state.shoppingCartItems![productId].quantity--;
            this.state.shoppingCartItems![productId].subtotal -= this.state.shoppingCartItems![productId].details.price; 
        }
        

        this.setState({
            shoppingCartItems: this.state.shoppingCartItems
        });
    }

    renderCheckoutSection() {
        if (!this.state.shoppingCartItems) {
            return null;
        }

        if (Object.keys(this.state.shoppingCartItems).length === 0) {
            return (
                <Link to = {"/list"}>
                    <div 
                        style={{
                            position: "relative",
                            left: "50%",
                            transform: "translate(-50%, 0%)",
                            width: "700px",
                            height: "500px",
                            paddingBottom: "16px",
                            marginBottom: "24px",
                            backgroundImage: 'url(https://cdn.dribbble.com/users/844846/screenshots/2981974/empty_cart_800x600_dribbble.png?compress=1&resize=800x600)',
                            backgroundRepeat: "no-repeat",
                            backgroundSize: "cover",
                    }}
                    />
                </Link>
            );
        }

        let total = 0;
        for (let productId of Object.keys(this.state.shoppingCartItems!)) {
            total += this.state.shoppingCartItems[productId].subtotal;
        }
        
        return (
            <section>
                <hr/>
                <h3 className = "price-tag">Total $ {total}</h3>
                <Button 
                    className = "checkoutButton"
                    variant="info" 
                    onClick = {() => this.onClickCheckOutButton()}
                    size="lg" block
                >
                    <b>Checkout: $ {total}</b>
                </Button>
            </section>
        )

    }

    onClickCheckOutButton(){
        console.log("whether login", AuthService.isLogin);
        if (!AuthService.isLogin()) {
            window.location.href = "#/login";
            return;
        }

        // always trust the data from bank-end then data in front-end
        const checkoutItems: ShoppingCartItem[] = [];
        // Get all Map key values
        for (let productId of Object.keys(this.state.shoppingCartItems!)) {
            checkoutItems.push({
                productId: productId,
                quantity: this.state.shoppingCartItems![productId].quantity
            });
        }
        // call Ext Api before changing page
        AuthService.getIdToken()
            .then((idToken) => {
                BackendExtService.checkout(idToken, checkoutItems, this.onCreatedTransaction);
            })
        
    }

    onCreatedTransaction(data: Transaction) {
        this.props.shoppingCartService.checkout();
        // browser build-in: window
        window.location.href = "#/checkout/" + data.transactionId;
    }
    
    render() {
        return (
            <Container id="shoppingCartPage">

                <Breadcrumb>
                        <Breadcrumb.Item href="#/list">
                            <FontAwesomeIcon icon={faGripHorizontal}/>
                            <b> Products List</b>
                        </Breadcrumb.Item>
                </Breadcrumb>
          
                <ProductList
                    displayItems = {this.state.shoppingCartItems}
                    shouldShowRemoveButton = {true}
                    onClickDeleteItemButton = {this.onClickDeleteItemButton}
                    shouldShowQuantityControls = {true}
                    addQuantity = {this.addQuantity}
                    minQuantity = {this.minQuantity}
                />

                {this.renderCheckoutSection()}     
            
            </Container>
        )
    }
}