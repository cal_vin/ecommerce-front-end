import React from 'react';
import { Card, CardColumns, Container, Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { ProductList } from '../../../domain/backendDos';
import BackendExtService from '../../../extService/BackendExtService';
import "./style.css";

// Props is to receive things given by others
type Props = {};
// State is to store things about UI variable/values
type State = {
    productList?: ProductList
    // "?" exist because at the moment of first rendering (conditional render), havn't call componentDidMount(), the productList is empty that time.
};

export default class ProductListingPage extends React.Component<Props,State> {
    state = {} as State;
    
    constructor(props: Props) {
        super(props);

        this.onLoadedProductList = this.onLoadedProductList.bind(this);
    }
    
    // trigger after render()
    componentDidMount() {
        BackendExtService.getProductList(this.onLoadedProductList);
    }

    onLoadedProductList(data: ProductList) {
        this.setState({
            productList: data
        });
    }

    renderProductItems() {
        // JSX: JavaScript XML
        const cards: JSX.Element[] = [];

        if (!this.state.productList) {
            return [];
        }

        for (let item of this.state.productList) {
            cards.push(
                // turn to detail page by react-router
                <Link to = {"/detail/" + item.id}>
                    <Card className = "productCard">

                        <Card.Img variant = "top" className = "image" style={{"backgroundImage" : "url(" + item.imageUrl + ")"}}/>
                        
                        <Card.Title className = "text">{item.name}</Card.Title>
                        
                        <Card.Footer>
                          <small className="footer">${item.price}</small>
                        </Card.Footer>

                    </Card>
                </Link>
            )
        }
        return cards;
    }

    render() {
        return (

            <Container id="productListingPage">
                
                <div className="inlineblock">
                    <div className = "productContainer">
                        {
                            (this.state.productList) ? this.renderProductItems() : (
                                <div>Loading...</div>
                            )
                        }
                    </div>
                </div>
                <div className="inlineblock"> 
                    <div className = "block">
                        <div className = "subBlock1"></div>
                        <div className = "subBlock2"></div>
                    </div>
                    
                </div>
  

            </Container>
        );
    }
}