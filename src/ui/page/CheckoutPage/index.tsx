import React from 'react';
import "./style.css";

import { Breadcrumb, Button, Col, Container, Form, Row } from 'react-bootstrap';

import { withRouter, RouteComponentProps } from 'react-router-dom';
import { PaymentInformation, ProductItem, Transaction, TransactionItem } from '../../../domain/backendDos';
import BackendExtService from '../../../extService/BackendExtService';
import AuthService from '../../../service/AuthService';
import ProductList from '../../component/ProductList';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGripHorizontal } from '@fortawesome/free-solid-svg-icons';
// must be index.tsx in ReactJS, Webpack setting, but style.css can be other name in lower letter

type RouterParmaProps = {
    transactionId: string
}
// withRounter give extra props (RouteComponentProps) to CheckoutPage 
type Props = RouteComponentProps<RouterParmaProps> & {};
type State = {
    transaction?: Transaction,
    billingInformation: {
        firstName: string,
        lastName: string,
        email: string,
        billingAddress: string
    }
};

// fully controlled component: two-way binding, user Input/UI influence state/memory, caange memory/state influence UI
class CheckoutPage extends React.Component<Props, State> {
    state = {
        billingInformation: {
            firstName: "",
            lastName: "",
            email: "",
            billingAddress: ""
        }
    } as State; // Initial State

    constructor(props: Props) {
        super(props);
        this.onLoadedTransaction = this.onLoadedTransaction.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onClickPaymentButton = this.onClickPaymentButton.bind(this);
    }

    componentDidMount() {
        AuthService.getIdToken()
            .then((idToken) => {
                BackendExtService.getTransaction(idToken, +this.props.match.params.transactionId, this.onLoadedTransaction);
            })
            .catch((e) => {
                window.location.href = "#/login";
                alert("Not signed in");
            })
    }
    
    onLoadedTransaction(data: Transaction) {
        this.setState({
            transaction: data
        });
        // when set cvv, password wouldn't disapper 
        // Only in the case of one layer (no object exist in the state)
        // because setState will automatically merger old state with overrided new value
    }

    renderProductList() {
        if (!this.state.transaction) return null;
        
        const items = this.state.transaction!.items;
        const checkoutItems: {[key: string]: TransactionItem} = {};
        for (let item of items) {
            checkoutItems[item.details.id] = item;
        }

        return (
            <section>
                <ProductList 
                    displayItems = {checkoutItems}
                    shouldShowRemoveButton = {false}
                    shouldShowQuantityControls = {false}
                />
                <h3 className = "price-tag">Total: ${this.state.transaction.total}</h3>
            </section>
        )
    }

    handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
        const target = event.target; // Input Text box, element
        const value = target.type === 'checkbox' ? target.checked : target.value; // user latest text box
        const name = target.name; // name of element
        // add annotation to ignore the potential error if the name of key didn't exist in the type of state definition
        // @ts-ignore
        this.setState((prevState) => (
            {
                billingInformation: {
                    ...prevState.billingInformation,
                    [name]: value
            }
          
        }));
        // short-cut for return a object in function: () => ( {Object} );
    }


    onClickReturnButton() {
        window.location.href = "#/list";
    }

    onClickPaymentButton() {
        let paymentInformation: PaymentInformation = {
            transactionId: this.state.transaction!.transactionId,
            billingInformation: this.state.billingInformation
        };

        AuthService.getIdToken()
            .then((idToken) => {
                BackendExtService.completeTransaction(idToken, paymentInformation, this.onCompletedTransaction);
        })
    }

    onCompletedTransaction(data: Transaction) {
        window.location.href = "#/complete";
    }
    


    render() {
        return (
            <Container id = "checkoutPage">
                <div>
                    <Breadcrumb>
                    
                            <Breadcrumb.Item href="#/list">
                                <FontAwesomeIcon icon={faGripHorizontal}/>
                                <b> Products List</b>
                            </Breadcrumb.Item>

                            <Breadcrumb.Item active>Checkout</Breadcrumb.Item>

                    </Breadcrumb>

                    <h3 className='heading'>Checkout</h3>

                    {this.renderProductList()}

                </div>

                <Form>
                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridFirstName">
                            <Form.Label>FirstName</Form.Label>
                            <Form.Control 
                                type="text" 
                                placeholder="Enter your firstname" 
                                name = "firstName"
                                onChange = {this.handleInputChange}
                                value = {this.state.billingInformation.firstName}
                            />
                        </Form.Group>

                        <Form.Group as={Col} controlId="formGridLastName">
                            <Form.Label>LastName</Form.Label>
                            <Form.Control 
                                type="text" 
                                placeholder="Enter your LastName" 
                                name = "lastName"
                                onChange = {this.handleInputChange}
                                value = {this.state.billingInformation.lastName}
                            />
                        </Form.Group>

                        <Form.Group as={Col} controlId="formGridEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control 
                                type="text" 
                                placeholder="Enter your email"
                                name = "email"
                                onChange = {this.handleInputChange}
                                value = {this.state.billingInformation.email} 
                            />
                        </Form.Group>

                        <Form.Group as={Col} controlId="formGridBillingAddress">
                            <Form.Label>BillingAddress</Form.Label>
                            <Form.Control 
                                type="text" 
                                placeholder="Enter your billingAddress" 
                                //for fully-controlled component
                                name = "billingAddress"
                                onChange = {this.handleInputChange}
                                value = {this.state.billingInformation.billingAddress}
                            />
                        </Form.Group>  
                    </Form.Row>

                    <Row>
                        <Col md = {6} xs = {6}>
                            <Button 
                                className = "button"
                                variant="outline-secondary"
                                size="lg"
                                onClick = {() => this.onClickReturnButton()}
                                block
                            >
                                <b>Decide Later . . .</b>
                            </Button>
                        </Col>
                        <Col md = {6} xs = {6} >
                            <Button 
                                className = "button"
                                variant="outline-success" 
                                size="lg"
                                onClick = {() => this.onClickPaymentButton()}
                                block
                            >
                                <b>Make Payment Now !</b>
                            </Button>
                        </Col>
                    
                    </Row>
                </Form>
            </Container>
        ); 
    
    
    }
}

export default withRouter(CheckoutPage);