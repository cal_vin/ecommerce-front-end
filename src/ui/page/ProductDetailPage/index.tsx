import { faGripHorizontal } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { Col, Form, Row } from 'react-bootstrap';
import { Breadcrumb, Button, Container, Toast } from 'react-bootstrap';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import { ProductItem } from '../../../domain/backendDos';
import { CommentDo, CommentDos, NewCommentDo } from '../../../domain/commentDos';
import { ToastMap, ToastItem } from '../../../domain/ToastItem';
import BackendExtService from '../../../extService/BackendExtService';
import AuthService from '../../../service/AuthService';
import ShoppingCartService from '../../../service/ShoppingCartService';
import QuantityControl from '../../component/QuantityControl';
import './style.css';

type RouterParmaProps = {
    productId: string
}

// need productId parmas in the match props given by react-router
type Props = RouteComponentProps<RouterParmaProps> & {
    // declare to receive the Instance from App.tsx
    shoppingCartService: ShoppingCartService
};

type State = {
    productDetail?: ProductItem,
    productComments?: CommentDos,
    toastMap: ToastMap,
    // isShowToastArray: boolean[];
    comment: string
};

// export: make the class public accessible
// default: import the default class when there are more than one class in a same file
class ProductDetailPage extends React.Component<Props, State> {
    state = {
        toastMap: {},
        comment: ""
    } as State;

    toastIndex: number = 0;

    quantityRef: React.RefObject<QuantityControl>;
    
    constructor(props: Props) {
        // super here is React.Component
        super(props);

        // call method by lifecycle no need to bind this
        this.onLoadedProductDetail = this.onLoadedProductDetail.bind(this);
        this.onLoadedProductComments = this.onLoadedProductComments.bind(this);
        this.onClickAddToCartButton = this.onClickAddToCartButton.bind(this);
        this.onCloseToast = this.onCloseToast.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onClickCreateCommentButton = this.onClickCreateCommentButton.bind(this);
        this.onCreatedComment = this.onCreatedComment.bind(this);

        this.quantityRef = React.createRef();
    }
    
    componentDidMount () {
        BackendExtService.getProductDetail(this.props.match.params.productId, this.onLoadedProductDetail)
        BackendExtService.getProductComments(this.props.match.params.productId, this.onLoadedProductComments)
    }

    onLoadedProductDetail(data: ProductItem) {
        this.setState({productDetail: data});
    }

    onLoadedProductComments(data: CommentDos) {
        this.setState({productComments: data});
    }

    renderProductDetail() { 
        const productDetail = this.state.productDetail!;

        return (
            <div>
                
                <Breadcrumb>
                        <Breadcrumb.Item href="#/list">
                            <FontAwesomeIcon icon={faGripHorizontal}/>
                                <b> Products List</b>
                        </Breadcrumb.Item>
                        <Breadcrumb.Item active>{productDetail.name}</Breadcrumb.Item>
                </Breadcrumb>
                    
                <Row>
                    
                    <Col md = {6} className="column image">
                        <img 
                            className = "bannerImage" 
                            style={{"backgroundImage" : "url(" + productDetail?.imageUrl + ")"}}
                        />

                    </Col>

                    <Col md = {6} className="column content">
                        <h1><b>{productDetail.name}</b></h1>

                        <p className="description">{productDetail.description}</p>

                        <div className= "priceSection">
                            <h3 className = "price"> $ {productDetail.price} / per</h3>
                            <h5 className = "notes">
                                *Select Quantity by Input OR Click Below (Max. 99)
                            </h5>
                        </div>

                        <div className="addCartSection">
                            {/* Pass QuantityControl Instance in the quantityRef variable*/}
                            <QuantityControl ref={this.quantityRef}/>

                            <Button 
                                className="addcart-button"
                                variant = "primary"
                                onClick = {this.onClickAddToCartButton} // this.onClickAddToCartButton is the variable contain the function
                                // equivalent to: () => this.onClickAddToCartButton()
                            >
                                Add to cart
                            </Button>

                        </div>
                    </Col>

                </Row>

            </div>
        );
    }

    onClickAddToCartButton() {
        const shoppingCartService = this.props.shoppingCartService;
        const productDetail = this.state.productDetail!; // "!" comfirm must not undefined this moment
        shoppingCartService.addToCart(productDetail.id , this.quantityRef.current!.getQuantity());
        this.AddToast();
    }


    AddToast() {
        const time:string = this.getFormattedDateTimeFromDataObject(new Date());
        const message:string = 
            "Woohoo, you're adding " + 
            this.quantityRef.current!.getQuantity() + " " + 
            this.state.productDetail?.name + 
            " in your shopping cart!";


        this.state.toastMap[this.toastIndex] = {
                time: time,
                message: message
            }
    
        this.toastIndex++;
        
        this.setState(
            {
                toastMap: this.state.toastMap
            }
        );        
        
    }

    onCloseToast(id: number) { 
        delete this.state.toastMap[id];

        this.setState({toastMap: this.state.toastMap});
    }

    renderToasts() {

        let toastArray: JSX.Element[] = [];
        for (let key of Object.keys(this.state.toastMap)) {
            toastArray.push(
                <Toast show = {true} onClose={() => this.onCloseToast(+key)} delay = {5000} autohide key={key}>
                    <Toast.Header>
                        {/* <img
                        src="holder.js/20x20?text=%20"
                        className="rounded mr-2"
                        alt=""
                        /> */}
                        <strong className="mr-auto">Message</strong>
                        <small>Time: {this.state.toastMap[+key].time}</small>
                    </Toast.Header>

                    <Toast.Body>
                        {this.state.toastMap[+key].message}
                    </Toast.Body>

                </Toast>
            );
        }

        return toastArray;
    }

    getFormattedDateTimeFromDataObject(dateObject: Date): string {
        // January is 0!
        // return ('0' + date.getDate()).slice(-2) + "/" + ('0' + (date.getMonth() + 1)).slice(-2) + "/" + date.getFullYear();
        const dd = String(dateObject.getDate()).padStart(2, '0');
        const mm = String(dateObject.getMonth() + 1).padStart(2, '0'); //January is 0!
        const h = String(dateObject.getHours()).padStart(2, '0');;
        var m = String(dateObject.getMinutes()).padStart(2, '0');;
        const time = h + ":" + m + ":00  ";
        const yyyy = dateObject.getFullYear();
        const date = dd + '/' + mm + '/' + yyyy;

        return time + date;
    }

    renderProductComments() {
        const comments: JSX.Element[] = [];

        if (this.state.productComments?.length === 0) {
            return (
                <h5 className="emptyCommentText">
                    Oh ~ there aren't any comments yet ~~ 
                    Being the first comment is pretty cool, isn't it ?      
                </h5>
            )
        }
        for (let comment of this.state.productComments!) {
            comments.push(
          
                <Row className="commentItem">
                    <Col md = {8} className="content">
                        <p>{comment.content}</p>
                    </Col>
                    <Col md = {4}>
                        <p className="writer-section-container">
                            <i>
                                written by {comment.writerName}
                                <br/> 
                                {"  time: " +  comment.time} 
                            </i>
                        </p>
                    </Col>
                </Row>
             
            )
        }

        return comments;
    }

    handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
        const target = event.target; // Input Text box, element
        const value = target.value; // user latest text box
        const name = target.name; // name of element

   
        if (value.length <= 255) {
            // add annotation to ignore the potential error if the name of key didn't exist in the type of state definition
            // @ts-ignore
            this.setState((prevState) => (
                {
                    [name]: value
                } 
            ))
        }
        // short-cut for return a object in function: () => ( {Object} );
    }

    renderFormSection() {
       return (
           <>
                <Form>
                    <Form.Label>* Max. 255 character</Form.Label>
                    <Form.Control 
                        as="textarea" 
                        rows={3} 
                        placeholder="Your comments here ..." 
                        name = "comment"
                        onChange = {this.handleInputChange}
                        value = {this.state.comment}
                    />
                </Form>
                <Button 
                    className="commentButton"
                    variant="success" 
                    size="lg"
                    onClick = {() => this.onClickCreateCommentButton()}
                    block
                >
                    <b>Make Comment Now !</b>
                </Button>
            </>
        )
   }

   onClickCreateCommentButton() {

        AuthService.getIdToken().
            then((idToken) => {
                // empty is false;
                if (!this.state.comment) {
                    alert("Hey ~ you should write something ~");
                    return;
                }

                let newComment : NewCommentDo = {
                    productId: this.props.match.params.productId,
                    content: this.state.comment,
                    time: this.getFormattedDateTimeFromDataObject(new Date())
                };

                BackendExtService.createComment(
                        idToken, 
                        newComment,
                        this.onCreatedComment
                    );
            })
            .catch((e) => {
                alert("Login to leave your comments !");
                window.location.href = "#/login";
            })
        
   }

   onCreatedComment(data: CommentDo) {
       console.log(data);
       BackendExtService.getProductComments(this.props.match.params.productId, this.onLoadedProductComments);
   }

    render() {

        return (
            <Container id = "productDetailPage">

                {
                    (this.state.productDetail) ? this.renderProductDetail() : (
                        <div>Loading Detail ...</div>
                    )
                }
                
                <h3 className="commentHeader">Leave Your Comment Below !</h3>

                {
                    (this.state.productComments) ? this.renderProductComments() : (
                        <div>Loading Comments ...</div>
                    )
                }

                {this.renderFormSection()}


                {
                    <div className = "toastContainer">
                        {this.renderToasts()}
                    </div>
                }

            </Container>
        );
    }
}

// use withRouter to wrap up the ProductDetailPage before its born
// react-route wrap many layer to its for providing information like path value by props
export default withRouter(ProductDetailPage);