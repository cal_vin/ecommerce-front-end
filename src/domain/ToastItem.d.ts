export type ToastItem = {
    time: string;
    message: string;
}

export type ToastMap = {
    [key: number]: ToastItem;
}