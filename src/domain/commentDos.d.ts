export type NewCommentDo = {
    productId: string,
    content: string,
    time: string
}

export type CommentDo = {
    commentId: number,
    productId: string,
    writerName: string,
    content: string,
    time: string
}

export type CommentDos = CommentDo[];