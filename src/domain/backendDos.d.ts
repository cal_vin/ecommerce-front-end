export type ProductItem = {
    id: string,
    name: string,
    price: number,
    description: string,
    imageUrl: string
};

export type ProductList = ProductItem[];

export type TransactionItem = {
    details: ProductItem,
    quantity: number,
    subtotal: number
}

export type TransactionItemMap = {
    [key: string] : TransactionItem
};

export type Transaction = {
    transactionId: number,
    items: TransactionItem[],
    total: number,
    status: "initiated" | "success" | "fail"
}

export type PaymentInformation = {
    transactionId: number,
    billingInformation: {
        firstName: string,
        lastName: string,
        email: string,
        billingAddress: string
    }
}