// Product API
export type ProductItemDto = {
    id: string,
    name: string,
    price: number,
    description: string,
    imageUrl: string
};

// fetchAllProduct
export type ProductListResponseDto = ProductItemDto[];
// fetchProductDetails
export type ProductDetailResponseDto = ProductItemDto;
// fetchProductsByIds
export type ShoppingCartItemsResponseDto = {
    [key: string]: ProductItemDto
};


// Transaction API
export type TransactionItemDto = {
    details: ProductItemDto,
    quantity: number,
    subtotal: number
}

export type TransactionDto = {
    transactionId: number,
    items: TransactionItemDto[],
    total: number,
    status: "initiated" | "success" | "fail"
}

export type CheckoutResponseDto = TransactionDto;
export type TransactionResponseDto = TransactionDto;
