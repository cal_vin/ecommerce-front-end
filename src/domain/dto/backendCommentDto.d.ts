export type CommentResponseDto = {
    commentId: number,
    productId: string,
    writerName: string,
    content: string,
    time: string
}

export type CommentResponseDtoList = CommentResponseDto[];


