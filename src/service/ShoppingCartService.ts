import React from 'react';
import { ShoppingCartItem } from '../domain/shoppingCartDos';
import LocalStorageUtil from '../util/LocalStorageUtil';

export default class ShoppingCartService {
    // use map to avoid loop the array to check duplicate when adding item
    shoppingCart: {
        [key: string] : ShoppingCartItem
    };
    shoppingCartKey = "cart";
    /*
    Map: index 555, 111
    Map can loop and find the matched index by tree algorithm more quiclkly
    Also it is more easy to write than looping Array in syntax
    {
        555: {
            productId: 555
            quantity: 3
        },
        111: {
            productId: 111
            quantity: 0
        }
    }

    V.S

    Array: index 0, 1
    [
        {
            productId: 555,
            quantity: 3
        },
        {
            productId: 111,
            quantity:0
        }
    ]
    */

    constructor() {
        this.shoppingCart = LocalStorageUtil.getValue(this.shoppingCartKey);
        if (!this.shoppingCart) {
            this.shoppingCart = {};
        }
    
    }

    addToCart(productId: string, quantity: number) {
        if (this.shoppingCart[productId]) {
            
            this.shoppingCart[productId] = {
                "productId": productId,
                "quantity": +this.shoppingCart[productId].quantity + quantity
            };

        } else {

            this.shoppingCart[productId] = {
                "productId": productId,
                "quantity": quantity
            };
        }

        console.log(this.shoppingCart);

        LocalStorageUtil.setValue(this.shoppingCartKey, this.shoppingCart);
    }

    incrementOnCart(productId: string) {
        this.shoppingCart[productId].quantity ++;

        LocalStorageUtil.setValue(this.shoppingCartKey, this.shoppingCart);
    }

    decrementOnCart(productId: string) {
        if (this.shoppingCart[productId].quantity > 1)  
        this.shoppingCart[productId].quantity -- ;

        LocalStorageUtil.setValue(this.shoppingCartKey, this.shoppingCart);
    }

    removeFromCart(productId: string) {
        delete this.shoppingCart[productId];

        LocalStorageUtil.setValue(this.shoppingCartKey, this.shoppingCart);
    }

    getCart() {
        return this.shoppingCart;
    }

    checkout() {
        this.shoppingCart = {};
        LocalStorageUtil.setValue(this.shoppingCartKey, this.shoppingCart);
    }
    
}