import firebase from 'firebase';
import React from 'react';
import { UserInfo } from '../domain/backendAuthDo';
import BackendAuthExtService from '../extService/BackendAuthExtService';

type onAuthStateChange = (isLoading: boolean) => void;
type onSigninSuccess = () => void;
type onSignInError = (code: string, message: string) => void;

type onRegistrySuccess = () => void;
type onRegistryError = (code: string, message: string) => void;

type OnSignOutSuccess = () => void;


export default class AuthService {
    
    // Class attributes no need to declare let/const
    // API token to call firebase, but can be used by others, not secure
    firebaseConfig = {
        apiKey: "AIzaSyBuRxzb_9YbeRMM1ft9TvmKZoZpVn7Rdwo",
        authDomain: "calvin-venturenix-project.firebaseapp.com",
        projectId: "calvin-venturenix-project",
        storageBucket: "calvin-venturenix-project.appspot.com",
        messagingSenderId: "528796546557",
        appId: "1:528796546557:web:513de0af72f969733f5594",
        measurementId: "G-42LWXSMGBN"
    };

    constructor(onAuthStateChange: onAuthStateChange) {
        this.onAuthStateChange = onAuthStateChange;
    }

    onAuthStateChange: onAuthStateChange;
    static signedInUserInfo?: UserInfo;
    static isLogin() {
        return AuthService.signedInUserInfo != undefined && AuthService.signedInUserInfo != null;
    }
    static isSigneIn() {
        // "!!"" => turn object to boolean, ! + object will turn object into boolean
        return !!this.signedInUserInfo;
    }
    static getIdToken() {
        if (!AuthService.isInitializing) {
            // getIdToken(false): if not expired get same Id token if flase
            // getIdToken(true): force to get a different Id token for each time call it, only useful for getting real-tiem updated user info.
            // get latest IdToken and return Promise, auto refresh the idToken if expired
            if (firebase.auth().currentUser)
                return firebase.auth().currentUser!.getIdToken(false); 
            else {
                return Promise.reject("not signed in");
            }
        } else {
            return new Promise<firebase.User | null>( (resolve, reject) => {
                const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
                    // call the function itself will terminate the onAuthStateChanged
                    unsubscribe();
                    resolve(user);
                })
            }).then((user) => firebase.auth().currentUser!.getIdToken(/* forceRefresh Login Status */false))
        }
    }

    static isInitializing: boolean = false;

    init() {
        AuthService.isInitializing = true;
        // Initial firebase app, init only run for one time
        if (!firebase.apps.length) {
            // Check any valid login session on firebase Server and local storage in the computer 
            // by the local firebase library in npm
            firebase.initializeApp(this.firebaseConfig);
        } else {
            firebase.app();
        }

        firebase.auth().onAuthStateChanged(
            // pass the function to local npm firebase library, 
            // Auto call the function if login status change 
            (user) => {
            AuthService.isInitializing = false; 
            console.log("onAuthStateChange: ", user)
            
            if (user) {
                // Signed In
                firebase.auth().currentUser!.getIdToken(false)
                    .then((idToken) => {
                        BackendAuthExtService.getUserInfo(idToken, (userInfo) => {
                            // run only login finish, put after ExtApi as callback
                            AuthService.signedInUserInfo = userInfo;
                            console.log("Login Success ! ", AuthService.signedInUserInfo.uid);
                            this.onAuthStateChange(false);
                        });
                    })
            } else {
                // Signed Out
                AuthService.signedInUserInfo = undefined;
                console.log("Logout Success ! ", AuthService.signedInUserInfo);
                this.onAuthStateChange(false);
            }
        })
    }

    signInWithEmailPassword(email: string, password: string, onSuccess: onSigninSuccess, onError: onSignInError) {
        // call app.tsx setState(), re-render to show loading page
        this.onAuthStateChange(true);
        firebase.auth().signOut()
            .then(() => {
                firebase.auth().signInWithEmailAndPassword(email, password)
            // if sucess (loginStatuschange), firbase will call firebase.auth().onAuthStateChanged auto
                    .then(() => {
                        onSuccess();
                    })
                    .catch((error) => {
                        console.log(error);
                        onError(error.code, error.message);
                        this.onAuthStateChange(false);
                    });
                    // handle error from Promise
            });
    }

    signInWithGoogle(onSuccess: onSigninSuccess, onError: onSignInError) {
        this.onAuthStateChange(true);
        
        firebase.auth().signOut()
            .then(() => {
                const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
                firebase.auth().signInWithPopup(googleAuthProvider)
                    .then(() => {
                        onSuccess();
                    })
                    .catch( (error) => {
                        console.log(error);
                        onError(error.code, error.message);
                        this.onAuthStateChange(false);
                    });
            });
        
    }

    signInWithFacebook(onSuccess: onSigninSuccess, onError: onSignInError) {
        this.onAuthStateChange(true);
        
        firebase.auth().signOut()
            .then(() => {
                const facebookAuthProvider = new firebase.auth.FacebookAuthProvider();
                firebase.auth().signInWithPopup(facebookAuthProvider)
                    .then(() => {
                        onSuccess();
                    })
                    .catch( (error) => {
                        console.log(error);
                        onError(error.code, error.message);
                        this.onAuthStateChange(false);
                    });
            });
        
    }

    signOut(onSuccess?: OnSignOutSuccess) {
            this.onAuthStateChange(true);
            firebase.auth().signOut()
                .then(() => {
                    if (onSuccess)
                        onSuccess();
                });
    }

    registryWithEmailPassword(email:string, password:string, onSuccess: onRegistrySuccess, onError: onRegistryError) {

        this.onAuthStateChange(true);

        firebase.auth().signOut()
            .then(() => {
                firebase.auth().createUserWithEmailAndPassword(email, password)
                    .then((userCredential) => {
                        // Signed in 
                        const user = userCredential.user;
                        alert("Registry Success ! Welcome " + user?.email)
                        onSuccess();
                    })
                    .catch((error) => {
                        const errorCode = error.code;
                        if (errorCode == 'auth/weak-password') {
                            alert('Password is too weak.' + error.message);
                            this.onAuthStateChange(false);
                        } else {
                            console.log(error);
                            onError(error.code, error.message);
                            this.onAuthStateChange(false);
                        }
                            
                    });
            });
        
    }


}